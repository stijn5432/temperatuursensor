/* 
 * File:   io_expander.h
 * Author: Hans Naert
 *
 * Created on 14 september 2013, 14:46
 */

#ifndef _IO_EXPANDER_H
#define	_IO_EXPANDER_H

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include "system_config.h"
#include "system_definitions.h"

void delay_us(uint32_t us);

void initializeIoExpander();

void setLeds(uint8_t value);

uint8_t getButtons();

#endif	/* IO_EXPANDER_H */

