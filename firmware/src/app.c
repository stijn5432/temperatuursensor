/*******************************************************************************
  MPLAB Harmony Application Source File
  
  Company:
    Microchip Technology Inc.
  
  File Name:
    app.c

  Summary:
    This file contains the source code for the MPLAB Harmony application.

  Description:
    This file contains the source code for the MPLAB Harmony application.  It 
    implements the logic of the application's state machine and it may call 
    API routines of other MPLAB Harmony modules in the system, such as drivers,
    system services, and middleware.  However, it does not call any of the
    system interfaces (such as the "Initialize" and "Tasks" functions) of any of
    the modules in the system or make any assumptions about when those functions
    are called.  That is the responsibility of the configuration-specific system
    files.
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END


// *****************************************************************************
// *****************************************************************************
// Section: Included Files 
// *****************************************************************************
// *****************************************************************************

#include "app.h"
#include "bsp_config.h"
#include "driver/tmr/drv_tmr_static.h"
#include "IoExpander.h"
// *****************************************************************************
// *****************************************************************************
// Section: Global Data Definitions
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
/* Application Data

  Summary:
    Holds application data

  Description:
    This structure holds the application's data.

  Remarks:
    This structure should be initialized by the APP_Initialize function.
    
    Application strings and buffers are be defined outside this structure.
*/

APP_DATA appData;

// *****************************************************************************
// *****************************************************************************
// Section: Application Callback Functions
// *****************************************************************************
// *****************************************************************************

/* TODO:  Add any necessary callback funtions.
*/

// *****************************************************************************
// *****************************************************************************
// Section: Application Local Functions
// *****************************************************************************
// *****************************************************************************

/* TODO:  Add any necessary local functions.
*/

uint32_t APP_ReadCoreTimer()
{
    uint32_t timer;

    // get the count reg
    asm volatile("mfc0   %0, $9" : "=r"(timer));

    return(timer);
}

void APP_StartTimer(uint32_t period)
{
    /* Reset the coutner */

    uint32_t loadZero = 0;

    asm volatile("mtc0   %0, $9" : "+r"(loadZero));
    asm volatile("mtc0   %0, $11" : "+r" (period));

}

void delay_us(uint32_t us) {
   unsigned coreTimer = APP_ReadCoreTimer();
   while ((APP_ReadCoreTimer() - coreTimer) < SYS_CLK_FREQ/1000000 * us);
}

void I2cStart(void)
{
DRV_I2C0_MasterStart();
DRV_I2C0_WaitForStartComplete();
}

void I2cReStart(void)
{
 DRV_I2C0_MasterRestart();
 DRV_I2C0_WaitForStartComplete();
}

void I2cStop (void)
{
 DRV_I2C0_MasterStop();
#if __PIC32MX
 DRV_I2C0_WaitForStopComplete();
#endif
}
uint8_t I2C_RxOneByte(bool Ack)
{
uint8_t DataByte;

DRV_I2C0_SetUpByteRead();
//while (!DRV_I2C0_WaitForReadByteAvailable()); //Set Rx enable in MSTR which causes SLAVE to send data
DataByte = DRV_I2C0_ByteRead();   //Read from I2CxRCV
if (Ack)
    DRV_I2C0_MasterACKSend();
else
    DRV_I2C0_MasterNACKSend();   //last byte; send NACK to Slave, no more data needed

return (DataByte);
}
void I2cWriteByte (uint8_t Val)
{
 if (DRV_I2C0_ByteWrite(Val))
 {
    DRV_I2C0_WaitForByteWriteToComplete();
 }
}//function

uint8_t TMPReadRegisters(uint8_t SLAVEADRESS,uint8_t RegisterAddress, int NumberBytesToRead)
{
    //printf("\r\nincode");
    int i;
    uint8_t Data;
    uint8_t Data2;
    
    while (!DRV_I2C0_MasterBusIdle());
    //printf("\r\nvoorstart");
    I2cStart();
   // printf("\r\ngestart");
    while (!DRV_I2C0_MasterBusIdle());
    I2cWriteByte(SLAVEADRESS);
  //  printf("\r\nslaveadress");
    while (!DRV_I2C0_MasterBusIdle());
    I2cWriteByte(0x00);
  //  printf("\r\nregisteradress");
    while (!DRV_I2C0_MasterBusIdle());
    I2cReStart();
    //printf("\r\nrestart");
    while (!DRV_I2C0_MasterBusIdle());
    I2cWriteByte(SLAVEADRESS| 0x01);
    while (!DRV_I2C0_MasterBusIdle());
    for (i = 0; i < NumberBytesToRead; i++)
    {
        if ((i + 1) < NumberBytesToRead)
        {
            //printf("\r\ndatabyte");
            
            Data = I2C_RxOneByte(true);
        }
        else
        {
         
            Data2 = I2C_RxOneByte(false); // last byte => NACK
            while (!DRV_I2C0_WaitForACKOrNACKComplete());
        }
    }
    while (!DRV_I2C0_MasterBusIdle());
    I2cStop();
//     printf("\r\nstop");
    return Data;
}

// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine Functions
// *****************************************************************************
// *****************************************************************************

/*******************************************************************************
  Function:
    void APP_Initialize ( void )

  Remarks:
    See prototype in app.h.
 */

void APP_Initialize ( void )
{
    /* Place the App state machine in its initial state. */
    appData.state = APP_STATE_INIT;
    
    /* TODO: Initialize your application's state machine and other
     * parameters.
     */
}


/******************************************************************************
  Function:
    void APP_Tasks ( void )

  Remarks:
    See prototype in app.h.
 */

void APP_Tasks ( void )
{
    /* Check the application's current state. */
    switch ( appData.state )
    {
        /* Application's initial state. */
        case APP_STATE_INIT:
        {
            printf("\r\nApp initialized");
            appData.state =APP_STATE_RUN;
            break;
        }
        case APP_STATE_RUN:
        {
          uint8_t data;
           printf("\r\nApp run");
           data = TMPReadRegisters(0x90,0x00,2);
           delay_us(2500000);
           printf("%d",data);
            break;
        }
        
        
        /* TODO: implement your application state machine.*/

        /* The default state should never be executed. */
        default:
        {
            /* TODO: Handle error in application's state machine. */
            break;
        }
    }
}
 

/*******************************************************************************
 End of File
 */
